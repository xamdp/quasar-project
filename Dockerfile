# Multi-stage build
# Use the Node.js Alpine image as the builder stage
FROM node:alpine as builder
# Set the working directory to /app in the builder stage
WORKDIR /app
# Copy the local files into the container's /app directory
COPY . .
# Install Node.js dependencies
RUN npm install
# Install Quasar CLI globally
RUN npm install -g @quasar/cli
# Build the Quasar application
RUN quasar build

# Start the second stage with a lightweight Nginx image
FROM nginx:alpine
# Set the working directory to Nginx's HTML directory
WORKDIR /usr/share/nginx/html
# Copy the built files from the builder stage to the Nginx stage
COPY --from=builder /app/dist/spa .
# Remove the default Nginx configuration
RUN rm /etc/nginx/conf.d/default.conf
# Copy your custom Nginx configuration
COPY nginx.conf /etc/nginx/conf.d/
# Expose port 80 for incoming HTTP traffic
EXPOSE 80
# Start Nginx with daemon off for foreground execution
CMD ["nginx", "-g", "daemon off;"]
